  <div class="block block-{$block->module}" id="block-{$block->module}-{$block->delta}">
    <h2 class="title">{$block->subject}</h2>
    <div class="content">{$block->content}</div>
 </div>
