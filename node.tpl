  <div class="node{if $sticky} sticky{/if}">
    {if $picture}{$picture}{/if}
    {if $page == 0}<h2 class="title"><a href="{$node_url}">{$title}</a></h2>{/if}
    <span class="submitted">{$submitted}</span>
    <span class="taxonomy">{$terms}</span>
    <div class="content">{$content}</div>
    <div class="links">&raquo; {$links}</div>
  </div>
