  <div class="comment">
    {if $picture}{$picture}{/if}
    <h3 class="title">{$title}</h3>{if $new != ''}<span class="new">{$new}</span>{/if}
    <div class="submitted">{$submitted}</div>
    <div class="content">{$content}</div>
    <div class="links">&raquo; {$links}</div>
  </div>
