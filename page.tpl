<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="{$language}" xml:lang="{$language}">

<head>
  <title>{$head_title}</title>
  {$head}
  {$styles}
  <script type="text/javascript">{* Needed to avoid Flash of Unstyle Content in IE *} </script>
</head>

<body{$onload_attributes}>

<table border="0" cellpadding="0" cellspacing="0" id="header">
  <tr>
    <td id="logo">

      {if isset($logo)}<a href="./" title="Home"><img src="{$logo}" alt="Home" border="0" /></a>{/if}
      {if isset($site_name)}<h1 class='site-name'><a href="./" title="Home">{$site_name}</a></h1>{/if}
      {if isset($site_slogan)}<div class='site-slogan'>{$site_slogan}</div>{/if}

    </td>
    <td id="menu">
      {if isset($secondary_links)}<div id="secondary">{theme function='links' data=$secondary_links}</div>{/if}
      {if isset($primary_links)}<div id="primary">{theme function='links' data=$primary_links}</div>{/if}
      {$search_box}
    </td>
  </tr>
  <tr>
    <td colspan="2"><div>{$header}</div></td>
  </tr>
</table>

<table border="0" cellpadding="0" cellspacing="0" id="content">
  <tr>
    {if $sidebar_left}<td id="sidebar-left">
      {$sidebar_left}
    </td>{/if}
    <td valign="top">
      {if $mission}<div id="mission">{$mission}</div>{/if}
      <div id="main">
        {$breadcrumb}
        <h1 class="title">{$title}</h1>
        <div class="tabs">{$tabs}</div>
        {$help}
        {$messages}

        {$content}
      </div>
    </td>
    {if $sidebar_right}<td id="sidebar-right">
      {$sidebar_right}
    </td>{/if}
  </tr>
</table>

<div id="footer">
  {$footer_message}
</div>
{$closure}
</body>
</html>
